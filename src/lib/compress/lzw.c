#include <strings.h>
#include "lzw.h"
#include "trie.h"
#include "../logger.h"
#include "../error.h"

// size of extended ASCII table
#define MAX_ALLOWED_CHARACTERS 256
#define MAX_SEQUENCE_SIZE 65536

void lzw_compress(FILE *input_file, FILE *output_file) {
    prints("Started LZW compression with 16 bit codes.\n");

    // reserve space to store the position in the file where codes will be stored using 24 bits (3 bytes)
    fseek(output_file, sizeof(long int), SEEK_SET);

    // reserve space to store the position in the file where codes will be stored using 32 bits (4 bytes)
    fseek(output_file, sizeof(long int), SEEK_CUR);

    // reserve space to store the value of the maximum generated code
    fseek(output_file, sizeof(u_int32_t), SEEK_CUR);

    // get the first byt from the input file
    unsigned char c = fgetc(input_file);
    if (feof(input_file)) {
        printf("%sError:%s Can not compress empty file.\n", COLOR_ERR, COLOR_NORMAL);
        exit(INPUT_FILE_EMPTY_EXIT_CODE);
    }

    // positions in the file where codes are stored using 24 bits and 32 bits
    long int file_offset_24bit_codes = 0, file_offset_32bit_codes = 0;

    // create dictionary (trie tree struct)
    TrieNode *tree = create_trie_node();

    // initialize code
    uint32_t current_code = 0;

    // initialize dictionary with single character strings
    for (current_code = 0; current_code < MAX_ALLOWED_CHARACTERS; ++current_code) {
        insert_char(tree, (unsigned char) current_code, current_code);
    }

    TrieNode *p;
    bool char_found = search_char(tree, c, &p);
    if (!char_found) {
        printf("%sError:%s File contains invalid characters and cannot be compressed.\n", COLOR_ERR, COLOR_NORMAL);
        exit(FILE_CONTAINS_INVALID_CHARACTERS_EXIT_CODE);
    }

    size_t bytes_per_code = 2; // 16 bits initially

    while (!feof(input_file)) {
        c = fgetc(input_file); // c = next input character

        TrieNode *p_plus_c;
        if (search_char(p, c, &p_plus_c)) { // if p + c is in the dictionary
            p = p_plus_c; // p = p + c
            continue;
        }

        insert_char(p, c, current_code++); // add p + c to the dictionary
        fwrite(&p->value, bytes_per_code, 1, output_file); // output the code for p
        char_found = search_char(tree, c, &p); // p = c
        if (!char_found) {
            printf("%sError:%s File contains invalid characters and cannot be compressed.\n", COLOR_ERR, COLOR_NORMAL);
            exit(FILE_CONTAINS_INVALID_CHARACTERS_EXIT_CODE);
        }

        if (current_code == 0xFFFF) { // 2^16 - 1
            bytes_per_code = 3; // 24 bits
            file_offset_24bit_codes = ftell(output_file);
            prints("LZW started using 24 bit codes.\n");
        } else if (current_code == 0xFFFFFF) { // 2^24 - 1
            bytes_per_code = 4; // 32 bits
            file_offset_32bit_codes = ftell(output_file);
            prints("LZW started using 32 bit codes.\n");
        }
    }

    fwrite(&p->value, bytes_per_code, 1, output_file);

    fseek(output_file, 0, SEEK_SET);
    fwrite(&file_offset_24bit_codes, sizeof(long int), 1, output_file);
    fwrite(&file_offset_32bit_codes, sizeof(long int), 1, output_file);
    fwrite(&current_code, sizeof(uint32_t), 1, output_file);

    free_trie_node(tree);
    prints("LZW compression finished successfully.\n");
}

/**
 * Determine the size of the next code to be read from the input file based on
 * the current position in the file and the offsets of 24-bit and 32-but codes.
 * @param code_size A pointer to the size variable that will be updated with the size of the next code (2, 3, or 4 bytes).
 * @param new  A pointer to the new variable that will be updated reset if needed.
 * @param input_file A pointer to the input file.
 * @param file_offset_24bit_codes The offset in bytes from the beginning of the input file where the 24-bit codes start. If 0, then 24-bit codes are not used.
 * @param file_offset_32bit_codes The offset in bytes from the beginning of the input file where the 32-bit codes start. If 0, then 32-bit codes are not used.
 */
void get_current_code_size(size_t *code_size, uint32_t *new, FILE *input_file, long int file_offset_24bit_codes,
                           long int file_offset_32bit_codes) {
    if ((ftell(input_file) < file_offset_24bit_codes) || (file_offset_24bit_codes == 0)) {
        *new = 0;
        *code_size = 2;
    } else if ((ftell(input_file) < file_offset_32bit_codes) || (file_offset_32bit_codes == 0)) {
        *new = 0;
        *code_size = 3;
    } else {
        *code_size = 4;
    }
}

void lzw_decompress(FILE *input_file, FILE *output_file) {
    prints("Started LZW decompression.\n");
    uint32_t total_codes;
    long int file_offset_24bit_codes, file_offset_32bit_codes;
    fread(&file_offset_24bit_codes, sizeof(long int), 1, input_file);
    fread(&file_offset_32bit_codes, sizeof(long int), 1, input_file);
    fread(&total_codes, sizeof(uint32_t), 1, input_file);

    // read the first code (uint16_t) from the input file
    uint32_t old = 0, new = 0, current_code;
    fread(&old, sizeof(uint16_t), 1, input_file);

    if (feof(input_file)) {
        printf("%sError:%s Can not decompress empty file.\n", COLOR_ERR, COLOR_NORMAL);
        exit(INPUT_FILE_EMPTY_EXIT_CODE);
    }

    // Declare the table
    char **table = (char **) calloc(total_codes, sizeof(char *)); // Initializes all allocated char pointers to 0 (NULL)
    if (table == NULL) {
        printf("%sError:%s Can not allocate memory for decompression table.\n", COLOR_ERR, COLOR_NORMAL);
        exit(CAN_NOT_DECOMPRESS_EXIT_CODE);
    }

    // Initialize table with single character strings
    for (current_code = 0; current_code < MAX_ALLOWED_CHARACTERS; current_code++) {
        table[current_code] = (char *) malloc(sizeof(char) + 1);
        if (table[current_code] == NULL) {
            printf("%sError:%s Can not allocate memory for decompression table.\n", COLOR_ERR, COLOR_NORMAL);
            exit(CAN_NOT_DECOMPRESS_EXIT_CODE);
        }
        table[current_code][0] = (char) current_code;
        table[current_code][1] = '\0';
    }

    if (old >= MAX_ALLOWED_CHARACTERS) {
        printf("%sError:%s File contains invalid characters and cannot be decompressed.\n", COLOR_ERR, COLOR_NORMAL);
        exit(FILE_CONTAINS_INVALID_CHARACTERS_EXIT_CODE);
    }

    fwrite(table[old], sizeof(char), 1, output_file); // output translation of OLD

    char c = '\n';
    char s[MAX_SEQUENCE_SIZE];
    size_t current_code_size;

    while (!feof(input_file)) {
        get_current_code_size(&current_code_size, &new, input_file, file_offset_24bit_codes, file_offset_32bit_codes);
        fread(&new, current_code_size, 1, input_file); // NEW = next input code

        if (table[new] == NULL) { // IF NEW is not in the string table
            sprintf(s, "%s%c", table[old], c); // S = translation of OLD + C
        } else {
            strcpy(s, table[new]); // S = translation of NEW
        }

        c = s[0]; // C = first character of S
        if (c == EOF) {
            break;
        }

        fwrite(s, strlen(s), 1, output_file); // output of S

        table[current_code] = (char *) malloc((sizeof(char) * strlen(table[old])) + 2);
        if (table[current_code] == NULL) {
            printf("%sError:%s Can not allocate memory for decompression table.\n", COLOR_ERR, COLOR_NORMAL);
            exit(CAN_NOT_DECOMPRESS_EXIT_CODE);
        }
        sprintf(table[current_code++], "%s%c", table[old], c); // translation of OLD + C to the string table
        old = new;                                             // OLD = NEW
    }

    // free allocated memory for table
    for (uint32_t code = 0; code < total_codes; code++) {
        if (table[code] != NULL) {
            free(table[code]);
        }
    }
    free(table);
    prints("LZW decompression finished successfully.\n");
}
