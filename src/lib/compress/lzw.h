#ifndef CSV_SERIALIZER_LZW_H
#define CSV_SERIALIZER_LZW_H

#include <stdio.h>

/**
 * Compress the contents of an input file using the LZW algorithm
 * and writes the result to an output file.
 * @param input_file a pinter to the input file, which should be open in 'r' mode.
 * @param output_file a pointer to the output file, which should be open in 'wb' mode.
 */
void lzw_compress(FILE *input_file, FILE *output_file);

/**
 * Decompress the contents of an input file that was compressed
 * with the LZW algorithm and writes the original data to an output file.
 * @param input_file a pointer to the input file, which should be open in 'rb' mode.
 * @param output_file a pointer to hte output file, which should be open in 'w' mode.
 */
void lzw_decompress(FILE *input_file, FILE *output_file);

#endif //CSV_SERIALIZER_LZW_H
