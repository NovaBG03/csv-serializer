#include "trie.h"

TrieNode *create_trie_node() {
    TrieNode *node = (TrieNode *) calloc(1, sizeof(TrieNode));
    for (unsigned i = 0; i < TRIE_NODE_CHILDREN_SIZE; ++i) {
        node->children[i] = NULL;
    }
    node->is_terminal = false;
    return node;
}

void free_trie_node(TrieNode *node) {
    for (unsigned i = 0; i < TRIE_NODE_CHILDREN_SIZE; ++i) {
        if (node->children[i] != NULL) {
            free_trie_node(node->children[i]);
        }
    }
    free(node);
}

TrieNode *insert_char(TrieNode *root, unsigned char character, uint32_t value) {
    unsigned index = (unsigned) character;
    if (root->children[index] == NULL) {
        root->children[index] = create_trie_node();
        TrieNode *node = root->children[index];
        node->is_terminal = true;
        node->value = value;
    }
    return root;
}

bool search_char(TrieNode *root, unsigned char character, TrieNode **node_found) {
    TrieNode *node = root->children[(unsigned) character];
    if (node != NULL && node->is_terminal) {
        *node_found = node;
        return true;
    }
    return false;
}
