#ifndef CSV_SERIALIZER_TRIE_H
#define CSV_SERIALIZER_TRIE_H

#include <stdlib.h>
#include <stdbool.h>

// size of extended ASCII table
#define TRIE_NODE_CHILDREN_SIZE 256

/**
 * A node in a Trie data structure.
 * Contains a value, an array of child nodes and a flag indicating
 * whether the node is a terminal node (represents the end of a string).
 */
typedef struct TrieNode TrieNode;

struct TrieNode {
    uint32_t value;
    TrieNode *children[TRIE_NODE_CHILDREN_SIZE];
    bool is_terminal;
};

/**
 * Allocates memory for a new TrieNode and initializes its fields to default values.
 * @return a pointer to the new TrieNode.
 */
TrieNode *create_trie_node();

/**
 * Recursively frees the memory allocated for a TrieNode and its children.
 * @param node a pointer to the TrieNode to be freed.
 */
void free_trie_node(TrieNode *node);

/**
 * Inserts a new TrieNode as a child of the given root node,
 * representing the given character and a value.
 * @param root the root node to insert the new node under.
 * @param character the character to associate with the new node.
 * @param value the value to associate with the new node.
 * @return a pointer to the new TrieNode.
 */
TrieNode *insert_char(TrieNode *root, unsigned char character, uint32_t value);

/**
 * Searches the children of the given root node for a specific character.
 * @param root the root node to search under.
 * @param character the character to search for.
 * @param node_found a pointer to a TrieNode pointer which will be set to the found node if it exists.
 * @return ture if the character is a child of the root node, false otherwise.
 */
bool search_char(TrieNode *root, unsigned char character, TrieNode **node_found);

#endif //CSV_SERIALIZER_TRIE_H
