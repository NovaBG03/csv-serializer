#include <stdio.h>
#include <stdarg.h>
#include "logger.h"

/**
 * Variable used to control the output of the prints macro.
 * If is_silent is true, the prints macro does not print anything.
 * If is_silent is false, the prints macro prints its arguments using printf.
*/
static bool is_silent = false; // s -> not required, mutes logs

void set_logger_silent(bool silent) {
    is_silent = silent;
}

void prints(char *format, ...) {
    if (!is_silent) {
        va_list args;
        va_start(args, format);
        vprintf(format, args);
        va_end(args);
    }
}