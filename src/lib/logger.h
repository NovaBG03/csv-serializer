#ifndef CSV_SERIALIZER_LOGGER_H
#define CSV_SERIALIZER_LOGGER_H

#include <stdbool.h>

// Preprocessor macros defining ANSI color codes for use in terminal output.
#define COLOR_NORMAL "\x1B[0m"
#define COLOR_RED "\x1B[1;31m"
#define COLOR_CYAN "\x1B[1;36m"

#define COLOR_INFO COLOR_CYAN
#define COLOR_ERR COLOR_RED

/**
 * Config the logger to either print or not print output to the console.
 * @param silent true if the logger should not print output to the console, false otherwise
 */
void set_logger_silent(bool silent);

/**
 * Print formatted output to the console, unless the logger is set to be silent.
 * @param format a printf-style format string
 * @param ... additional arguments to be printed, corresponding to the format
 */
void prints(char *format, ...);

#endif //CSV_SERIALIZER_LOGGER_H
