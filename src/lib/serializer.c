#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "serializer.h"
#include "logger.h"
#include "error.h"
#include "compress/lzw.h"

/**
 * Returns the file path without extension.
 * @param path The path to remove extension from.
 * @return A pointer to the file path without extension.
 */
char *get_path_without_extension(char *path) {
    char *new_path = strdup(path);

    char *dot = strrchr(new_path, '.');
    if (dot != NULL) {
        *dot = '\0';
    }

    return new_path;
}

/**
 * Checks if a file has the correct type.
 * @param filename The name of the file to check.
 * @param type A string containing the correct file type.
 * @return true if the file has the correct type, and false otherwise.
 */
bool is_file_correct_type(char *filename, char *type) {
    size_t filename_length = strlen(filename);
    size_t type_length = strlen(type);

    if (filename_length < type_length) {
        return false;
    }

    if (strcmp(&filename[filename_length - type_length], type) != 0) {
        return false;
    }

    return true;
}

void serialize_csv(char *filename) {
    if (!is_file_correct_type(filename, ".csv")) {
        printf("%sError:%s Invalid file type for file. Must be a %sCSV file%s.\n", COLOR_ERR, COLOR_NORMAL, COLOR_INFO,
               COLOR_NORMAL);
        exit(INVALID_INPUT_FILE_TYPE_EXIT_CODE);
    }

    FILE *csv_f, *bin_f;
    prints("CSV serialization started.\n");

    prints("Opening %s\n", filename);
    if ((csv_f = fopen(filename, "r")) == NULL) {
        printf("%sError:%s File %s%s%s can not be opened or does not exist.\n", COLOR_ERR, COLOR_NORMAL, COLOR_INFO,
               filename,
               COLOR_NORMAL);
        fclose(csv_f);
        exit(CAN_NOT_OPEN_FILE_EXIT_CODE);
    }

    char *output_filename = strcat(get_path_without_extension(filename), ".bin");
    prints("Creating %s\n", output_filename);
    if ((bin_f = fopen(output_filename, "wb")) == NULL) {
        printf("%sError:%s Output file %s%s%s can not be created.\n", COLOR_ERR, COLOR_NORMAL, COLOR_INFO,
               output_filename,
               COLOR_NORMAL);
        fclose(csv_f);
        fclose(bin_f);
        exit(CAN_NOT_OPEN_FILE_EXIT_CODE);
    }

    lzw_compress(csv_f, bin_f);

    prints("Closing files.\n");
    fclose(csv_f);
    fclose(bin_f);
    prints("CSV serialized successfully.\n");
    prints("Output file: %s\n", output_filename);
}

void deserialize_csv(char *filename) {
    if (!is_file_correct_type(filename, ".bin")) {
        printf("%sError:%s Invalid file type for file. Must be a %sBinary file%s.\n", COLOR_ERR, COLOR_NORMAL,
               COLOR_INFO,
               COLOR_NORMAL);
        exit(INVALID_INPUT_FILE_TYPE_EXIT_CODE);
    }

    FILE *csv_f, *bin_f;
    prints("CSV deserialization started.\n");

    prints("Opening %s\n", filename);
    if ((bin_f = fopen(filename, "rb")) == NULL) {
        printf("%sError:%s File %s%s%s can not be opened or does not exist.\n", COLOR_ERR, COLOR_NORMAL, COLOR_INFO,
               filename,
               COLOR_NORMAL);
        fclose(bin_f);
        exit(CAN_NOT_OPEN_FILE_EXIT_CODE);
    }

    char *output_filename = strcat(get_path_without_extension(filename), "-gen.csv");
    prints("Creating %s\n", output_filename);
    if ((csv_f = fopen(output_filename, "w")) == NULL) {
        printf("%sError:%s Output file %s%s%s can not be created.\n", COLOR_ERR, COLOR_NORMAL, COLOR_INFO,
               output_filename,
               COLOR_NORMAL);
        fclose(bin_f);
        fclose(csv_f);
        exit(CAN_NOT_OPEN_FILE_EXIT_CODE);
    }

    lzw_decompress(bin_f, csv_f);

    prints("Closing files.\n");
    fclose(bin_f);
    fclose(csv_f);
    prints("CSV deserialized successfully.\n");
    prints("Output file: %s\n", output_filename);
}
