#ifndef CSV_SERIALIZER_SERIALIZER_H
#define CSV_SERIALIZER_SERIALIZER_H

/**
 * Serialize data from a CSV file to a newly created binary file
 * with the same name as the CSV file but with a `.bin` extension.
 * @param filename the name of the csv file to read from.
 */
void serialize_csv(char *filename);

/**
 * Deserialize data from a binary file to a newly created CSV file
 * with the same name but ending on '-gen.csv'.
 * @param filename the name of the binary file to read from.
 */
void deserialize_csv(char *filename);

#endif //CSV_SERIALIZER_SERIALIZER_H
