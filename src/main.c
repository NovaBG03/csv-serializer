#include <stdio.h>
#include <getopt.h>
#include <string.h>
#include "lib/logger.h"
#include "lib/error.h"
#include "lib/serializer.h"

#define SERIALIZE_KEY "serialize"
#define DESERIALIZE_KEY "deserialize"

/* Usage: svg-serializer -f <filename> -c <serialize/deserialize> [-s]
 *
 * Options:
 *  -f filename                     Input file path
 *  -c serialize or deserialize     Command mode
 *  -s                              Disable logs
 */
int main(int argc, char **argv) {
    char *filename = NULL; // f -> required
    char *command = NULL; // c -> required, serialize or deserialize

    int opt;
    while ((opt = getopt(argc, argv, ":sf:c:")) != -1) {
        switch (opt) {
            case 's':
                set_logger_silent(true);
                break;
            case 'f':
                filename = optarg;
                break;
            case 'c':
                command = optarg;
                break;
            case ':':
                printf("%sError:%s The option %s-%c%s requires ", COLOR_ERR, COLOR_NORMAL, COLOR_INFO, optopt,
                       COLOR_NORMAL);
                if (optopt == 'f') {
                    printf("a %sfull path to file%s as an argument.\n", COLOR_INFO, COLOR_NORMAL);
                } else if (optopt == 'c') {
                    printf("a %s%s%s or %s%s%s as an argument.\n", COLOR_INFO, SERIALIZE_KEY, COLOR_NORMAL, COLOR_INFO,
                           DESERIALIZE_KEY, COLOR_NORMAL);
                } else {
                    printf("an argument.\n");
                }
                return INVALID_ARGS_EXIT_CODE;
            case '?':
                printf("%sError:%s Invalid option %s-%c%s. Please provide a valid command line options.\n", COLOR_ERR,
                       COLOR_NORMAL, COLOR_INFO, optopt, COLOR_NORMAL);
                return INVALID_ARGS_EXIT_CODE;
            default:
                break;
        }
    }

    if (!filename) {
        printf("%sError:%s The option %s-f%s is required to specify a %sfull path to file%s.\n", COLOR_ERR,
               COLOR_NORMAL, COLOR_INFO,
               COLOR_NORMAL, COLOR_INFO, COLOR_NORMAL);
        return INVALID_ARGS_EXIT_CODE;
    }

    if (!command) {
        printf("%sError:%s The option %s-c%s is required to specify the operation %scommand%s, which can only be either %s%s%s or %s%s%s.\n",
               COLOR_ERR, COLOR_NORMAL, COLOR_INFO,
               COLOR_NORMAL, COLOR_INFO, COLOR_NORMAL, COLOR_INFO, SERIALIZE_KEY, COLOR_NORMAL, COLOR_INFO,
               DESERIALIZE_KEY, COLOR_NORMAL);
        return INVALID_ARGS_EXIT_CODE;
    }

    if (strcmp(command, SERIALIZE_KEY) == 0) {
        serialize_csv(filename);
    } else if (strcmp(command, DESERIALIZE_KEY) == 0) {
        deserialize_csv(filename);
    } else {
        printf("%sError:%s The option %s-c%s requires a %s%s%s or %s%s%s as an argument.\n", COLOR_ERR,
               COLOR_NORMAL, COLOR_INFO, COLOR_NORMAL, COLOR_INFO, SERIALIZE_KEY, COLOR_NORMAL, COLOR_INFO,
               DESERIALIZE_KEY, COLOR_NORMAL);
        return INVALID_ARGS_EXIT_CODE;
    }

    return 0;
}
